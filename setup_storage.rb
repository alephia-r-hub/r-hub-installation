require_relative('apt')

def setup_storage_for_master(label)
  Apt.new().update().install('exfat-fuse').done()
  
  storage_path = '/media/storage'
  setup_storage_path(storage_path)

  return if (label.empty?)

  Fstab.new().add_labeled(
    device: "LABEL=#{label}",
    target: storage_path,
    fs: Fstab.auto,
    options: [Fstab.rw, Fstab.auto, Fstab.nouser, Fstab.noexec],
    dump_frequency: 0,
    fscheck_pass_number: 2
  ).done()
end

def setup_storage_for_slave(user, master_host)
  Apt.new().update().install('sshfs').done()

  storage_path = '/media/storage'
  setup_storage_path(storage_path)

  Fstab.new().add(
    device: "#{user}@#{master_host}:#{storage_path}",
    target: storage_path,
    fs: Fstab.sshfs,
    options: [
      'x-systemd.automount', '_netdev', 'user', 'idmap=user', 'transform_symlinks',
      "identityfile=/home/#{user}/.ssh/id_rsa", 'allow_other', 'default_permissions'
    ],
    dump_frequency: 0,
    fscheck_pass_number: 0
  ).done()
end

def setup_storage_path(storage_path)
  Dir.mkdir(storage_path) unless Dir.exist?(storage_path)
end

class Fstab
  def self.rw
    'rw'
  end

  def self.auto
    'auto'
  end

  def self.sshfs
    'fuse.sshfs'
  end

  def self.nouser
    'nouser'
  end

  def self.noexec
    'noexec'
  end

  def initialize
    @entries = []
  end

  def add(device:, target:, fs:, options:, dump_frequency:, fscheck_pass_number:)
    @entries.append("#{device}\t#{target}\t#{fs}\t#{options.join(',')}\t#{dump_frequency}\t#{fscheck_pass_number}")
    self
  end

  def done()
    open('/etc/fstab', 'a') { |fstab| fstab << @entries.join("\n") }
  end
end
