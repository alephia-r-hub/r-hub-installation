#!/usr/bin/ruby

require_relative 'setup_section.rb'
require_relative 'read_configuration.rb'
require_relative 'setup_hostname.rb'
require_relative 'setup_user.rb'
require_relative 'setup_ssh.rb'
require_relative 'setup_docker.rb'
require_relative 'setup_fish.rb'
require_relative 'setup_gitlab_runner.rb'
require_relative 'setup_storage.rb'
require_relative 'setup_ssh_identity.rb'
require_relative 'setup_avahi.rb'
require_relative 'setup_k3s.rb'

puts ""
puts ""

if (ARGV.length == 0)
  configuration = setup_section('Configuration') { read_configuration }
  setup_section('Setup Hostname') { setup_hostname(configuration[:hostname]) }
  setup_section('Setup User') { setup_user(configuration[:user], configuration[:pubkey]) }
  setup_section('Setup SSH') { setup_ssh }
  setup_section('Setup Docker') { setup_docker(configuration[:user]) }
  setup_section('Setup Fish') { setup_fish(configuration[:user]) }
  setup_section('Setup Gitlab Runner') { install_gitlab_runner }
  setup_section('Setup Storage') {
     configuration[:master_host] == 'Yes' ? setup_storage_for_master('terpsi_storage') : setup_storage_for_slave(configuration[:user], configuration[:master_host])
  }
  setup_section('Setup SSH identity') { setup_ssh_identity(configuration[:user]) }
  setup_section('Setup avahi mDNS') { setup_avahi }
  setup_section('Setup k3s') { configuration[:master_host] == 'Yes' ? setup_k3s : do_nothing }
else
  case ARGV[0]
    when 'hostname'
      setup_section('Setup Hostname') { setup_hostname(ARGV[1]) }
    when 'user'
      setup_section('Setup User') { setup_user(ARGV[1], ARGV[2]) }
    when 'ssh'
      setup_section('Setup SSH') { setup_ssh(ARGV[1]) }
    when 'docker'
      setup_section('Setup Docker') { setup_docker(ARGV[1]) }
    when 'fish'
      setup_section('Setup Fish') { setup_fish(ARGV[1]) }
    when 'install_gitlab_runner'
      setup_section('Setup Gitlab Runner') { install_gitlab_runner }
    when 'setup_gitlab_runner'
      setup_section('Register Gitlab Runner') { setup_gitlab_runner(ARGV[1], %x(uname -n).gsub(/\n/, '')) }
    when 'setup_slave_storage'
      setup_section('Setup Storage') { setup_storage_for_slave(ARGV[1], ARGV[2]) }
    when 'setup_master_storage'
      setup_section('Setup Storage') { setup_storage_for_master(ARGV[1]) }
    else
      "Error: invalid setup choice"
  end
end

puts "R Hub installed!"
