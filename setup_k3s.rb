require_relative('inform_result.rb')

def setup_k3s
  %x(curl -sfL https://get.k3s.io | sh -)
  inform_result('k3s installed', 'could not install k3s')

  %x(curl -sLS https://get.k3sup.dev | sh)
  inform_result('k3sup fetched', 'could not fetch k3sup')
  
  %x(sudo install k3sup /usr/local/bin/)
  inform_result('k3sup installed', 'could not install k3sup')
end
