def setup_hostname(name)
  File.open('/etc/hostname', 'w') { |file| file.puts(name) }
  puts "[Success]: Hostname set up to #{name}"
  puts ""

  ConfigurationFile.open('/etc/avahi/avahi-daemon.conf')
    .edit(starting_with: '#cache-entries-max') { |line| 'cache-entries-max=0' }
    .save()
end
