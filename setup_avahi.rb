
def setup_avahi
  ConfigurationFile.open('/etc/avahi/avahi-daemon.conf')
  .edit(starting_with: '#allow-interfaces') { |line| 'allow-interfaces=wlan0' }
  .edit(starting_with: 'cache-entries') { |line| 'cache-entries-max=4096' }
  .save()
end
