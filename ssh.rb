require 'fileutils'
require_relative 'inform_result.rb'

def add_ssh_identity(user)
  %x(echo y | ssh-keygen -t rsa -f #{ssh_key_path_of(user)} -N "")
  inform_result("SSH identity created for #{user}", "Could not create SSH identity for #{user}")

  %x(chown -R #{user}:#{user} #{ssh_directory_of(user)})
  inform_result("SSH configuration chowned for #{user}", "Could not chown SSH configuration for #{user}")
end

def public_ssh_key(user)
    File.readlines(public_ssh_key_path_of(user))
end

def ssh_key_path_of(user)
  "#{ssh_directory_of(user)}/id_rsa"
end

def public_ssh_key_path_of(user)
  "#{ssh_directory_of(user)}/id_rsa.pub"
end
    
def ssh_directory_of(user)
  ssh_directory = "/home/#{user}/.ssh"
  FileUtils.mkpath(ssh_directory) unless Dir.exist?(ssh_directory)
  ssh_directory
end
