require_relative 'configuration_file.rb'

def setup_ssh
  %x(sudo systemctl enable ssh)
  inform_result("sshd enabled", "sshd not enabled")

  %x(sudo systemctl start ssh)
  inform_result("sshd started", "sshd not started")

  ConfigurationFile.open('/etc/ssh/sshd_config')
    .edit(starting_with: 'AcceptEnv') { |line| '' }
    .save()
  
  puts("")
end
