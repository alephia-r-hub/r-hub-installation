#!/bin/bash

apt-get install -y ruby git
git clone https://gitlab.com/alephia-r-hub/r-hub-installation.git
cd r-hub-installation
git pull
ruby run_installation.rb
