def read_configuration
  {
    hostname: read_config('Hostname', 'r-hub'),
    user: read_config('Your user', 'row'),
    pubkey: read_config("Your user's public key url", 'https://gitlab.com/alephia-r-hub/r-hub-installation/-/raw/master/row.alephia.tech.pub'),
    master_host: read_config('Does this host have a master?', 'No')
  }
end

def read_config(prompt, default=nil)
  print "  · #{prompt} [#{default.nil? ? 'none' : default}]: "
  input = gets.chomp

  input.empty? ? default : input 
end
