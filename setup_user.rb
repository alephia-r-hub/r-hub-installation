require 'net/http'
require 'uri'
require 'fileutils'
require_relative 'inform_result.rb'
require_relative 'ssh.rb'

def setup_user(user, pubkey_url)
  create_user(user)
  create_group('docker')
  add_groups(user, pi_groups.append('docker'))
  enable_sudo(user)
  setup_authorized_key(pubkey_url, user)
  puts("")
end

def create_user user
  %x(adduser --disabled-password --gecos "" #{user})
  inform_result("User [#{user}] created.", "Could not create user [#{user}]. Was the user already created?")
end

def create_group group
  %x(groupadd #{group})
  inform_result("Group [#{group}] created.", "Could not create group [#{group}]. Was the group already created?")
end

def add_groups(user, groups)
  %x(usermod -G #{groups.join(',')} #{user})
  inform_result("User [#{user}] added to groups.", "Could not add user [#{user}] to groups.")
end

def pi_groups
  %x(groups pi)
    .split(' ')
    .drop(2)
    .select { |element| element != "pi" }
end

def enable_sudo(user)
  sudoers = "/etc/sudoers.d/#{user}"
  File.open(sudoers, 'w') { |file| file.write("#{user} ALL=(ALL) NOPASSWD: ALL") }
  File.chmod(0440, "#{sudoers}")
  FileUtils.chown('root', 'root', sudoers)
end

def setup_authorized_key(pubkey_url, user)
  pubkey = fetch(pubkey_url)

  if !pubkey.nil?
    authorized_keys = "#{ssh_directory_of(user)}/authorized_keys"
    File.open(authorized_keys, 'a') { |file| file.write(pubkey) }
    puts("[Success]: Added an authorized key to [#{user}] for remote login.")
  else
    puts("[Fail]: Could not add an authorized key to [#{user}] for remote login.")
  end
end

def fetch(url)
  response = Net::HTTP.get_response(URI.parse(url))
  response.code == "200" ? response.body : nil
end
