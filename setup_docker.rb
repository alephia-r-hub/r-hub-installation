require_relative 'configuration_file.rb'

def setup_docker(user)
  %x(curl -fsSL https://get.docker.com -o get-docker.sh)
  inform_result("Docker install script fetched.", "Could not fetch Docker install script")

  %x(sh get-docker.sh)
  inform_result("Docker installed.", "Could not install Docker")

  %x(systemctl enable docker)
  inform_result("Docker enabled", "Could not enable Docker")

  open_tcp_socket

  %(systemctl daemon-reload)
  inform_result("Units reloaded", "Failed to reload units")

  %x(systemctl start docker)
  inform_result("Docker started", "Could not start Docker")
end

def open_tcp_socket
  ConfigurationFile.open(docker_configuration)
    .edit(starting_with: 'ExecStart') { |line| "#{line} -H tcp://0.0.0.0:2375" }
    .save()
end

def docker_configuration
  '/etc/systemd/system/multi-user.target.wants/docker.service'
end
